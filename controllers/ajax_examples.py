from random import randint
def index():
    return dict()

def data():
    if not session.m or len(session.m) == 10:
        session.m = []
    if request.vars.q: session.m.append(request.vars.q)
    session.m.sort()
    return TABLE(*[TR(v) for v in session.m]).xml()


def flash():
    response.flash = 'this text should appear!'
    return dict()


def fade():
    return dict()

def start():
    session.tikslas = randint(1, 10)
    session.spejimai = [ ]
    return A("Try guessing", _href="index")

def speliones():
    if session.spejimai is None:
        redirect('start')

    rez2 = "try again"
    if request.vars.kiek:  # None, ""  veikia kaip Fasle
        kiek = int(request.vars.kiek)
        session.m = []
        if request.vars.kiek: session.m.append(request.vars.kiek)
        session.m.sort()
        # papildom spejimus
        if kiek == session.tikslas:
            rez2 = "You got it"
        elif abs(kiek - session.tikslas) >= 5 :
            rez2 = "Wrong"
        elif kiek < session.tikslas:
            rez2 = "Too much"
        elif kiek > session.tikslas:
            rez2 = "Too few"

    return UL(rez2, UL(session.spejimai)).xml()
