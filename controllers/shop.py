def index():
    redirect(URL('prekes'))


# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


def populate_fresh_db():
    from gluon.contrib.populate import populate
    db.products.insert(name='pienas',price=2)
    db.products.insert(name='duona',price=0.9)
    db.products.insert(name='desra',price=5)
    db.products.insert(name='iphone', price=1000)

    populate(db.purchases, 10)

    return dict(
        purchases=db(db.purchases).select(),
        users=db(db.auth_user).select(),
        products=db(db.products).select(),
    )


def beautify_products(rows):
    product_ids = [x.products for x in rows]
    product_names = db(db.products.id.belongs(product_ids)).select()
    product_name = {x.id: x.title for x in product_names}
    result = UL(
        [LI(
            DIV(H5(product_name[x.product_id]),
                B(x.name[0:40]),  
                BR(), x.price,
                ),
           )
            for x in rows]
    )
    return result


def prekes():
    rows = UL(
        db().select(db.products.ALL),  
    A("Krepselin", _class='button btn btn-info', _href=URL('krepsys'))
    )
    return dict(rows=rows 
                )
def krepsys():

    return
